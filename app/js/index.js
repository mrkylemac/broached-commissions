import "./imports";

// move scrolling divs
$(function scrollContent() {
  $(window).on('load', function() {

    const $wraps = $('.move-me');

    const damp = 50; // Mousemove response softness
    const mousePadding = 200; // mousemove padding

    $wraps.each( function() {
      let $this = $( this );
      let outerWidth = $this.outerWidth ( true );
      let scrollWidth = $this[0].scrollWidth;
      let widthDiff = ( scrollWidth / outerWidth ) - 1; // widths difference ratio
      let mouseX = 0; // real mouse position
      let mouseX2 = 0; // modified mouse position
      let positionX = 0;
      let mousemoveAvailableArea = outerWidth - ( mousePadding * 2 ); // the mousemove available area
      let mousemoveAvailableAreaDiff = ( outerWidth / mousemoveAvailableArea ); // get available mousemove diference ratio

				if( window.innerWidth >= 768 ){
					$this.mousemove( function ( event ) {
						mouseX = event.pageX; //- $(this).parent().offset().left - this.offsetLeft
						mouseX2 = Math.min( Math.max ( 0, mouseX - mousePadding), mousemoveAvailableArea ) * mousemoveAvailableAreaDiff;
						positionX = mouseX2
						//positionX += (mouseX2 - positionX) / damp; // zeno's paradox equation "catching delay"
						let percent = mouseX / outerWidth * 90;

						$this.css({
							transform:'translateX(-' + (positionX * widthDiff) + 'px)'
							// transform:'translateX(-' + (percent * widthDiff) + '%)'
						});

					});
				}
		});
	});
});

// clocks
function runClock() {

  var now = new Date()

  var hour = now.getHours() % 12
  var min  = now.getMinutes()
  var sec  = now.getSeconds()
  var ms   = now.getMilliseconds()

  var clocks = document.querySelectorAll("div.clock")

  clocks.forEach(item => {
	  var hourHand = item.querySelector("div.hour")
	  var minHand  = item.querySelector("div.minute")
	  var secHand  = item.querySelector("div.second")

	  var hourRotation = 30 * hour + (0.5 * min)
	  var minRotation = 6 * min + (0.1 * sec)
	  var secRotation = 6 * sec + 0.006 * ms

	  hourHand.style.transform = "rotate(" + hourRotation + "deg)"
	  minHand.style.transform = "rotate(" + minRotation + "deg)"
	  secHand.style.transform = "rotate(" + secRotation + "deg)"
  })

  requestAnimationFrame(runClock)
}
runClock()
